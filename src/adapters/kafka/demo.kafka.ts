/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-console */
import { CompressionTypes } from 'kafkajs';
import { Service } from 'typedi';
import { kafkaConsumer } from '../../libraries/kafka/decorators/kafkaConsumer';
import { kafkaProducer } from '../../libraries/kafka/decorators/kafkaProducer';
import {
  RecoverableError,
  UnrecoverableError,
} from '../../libraries/kafka/errorTypes';
import {
  ConsumeMessage,
  ProduceMessage,
} from '../../libraries/kafka/kafkaBroker';

const counter = 0;

export interface IOrderSample {
  _id?: string;
  address?: string;
}

@Service()
export class KafkaDemoService {
  @kafkaProducer('tel-topic')
  async sendProducerSimple(order: IOrderSample): Promise<any> {
    return order;
  }

  @kafkaProducer({
    topic: `tel-topic`,
    config: {
      allowAutoTopicCreation: true,
    },
    options: {
      compression: CompressionTypes.GZIP,
      timeout: 10000,
      acks: -1,
    },
  })
  async sendProducerSimpleWithKeyAndHeaders(
    order: IOrderSample,
  ): Promise<ProduceMessage> {
    return {
      key: { 'test-with-json': 'test' },
      headers: {
        'x-kafka-number': `${Math.random() * 10000}`,
        'x-kafka-producer-from-service': 'ts-blueprint',
      },
      message: order,
    };
  }

  @kafkaProducer(`tel-topic-error`)
  async sendProducerSimpleError(order: IOrderSample): Promise<any> {
    return order;
  }

  @kafkaProducer(`tel-topic-error-dead-letter`)
  async sendProducerSimpleErrorDeadLetter(order: IOrderSample): Promise<any> {
    return order;
  }

  /** * Consumer simple configuration and usage RETRY strategy default */
  @kafkaConsumer({
    groupId: `tel-topic-group`,
    topic: `tel-topic`,
    subscribe: {
      fromBeginning: true,
    },
  })
  async receiveConsumerSimple(msg: ConsumeMessage) {
    console.log('🦻🦻🦻 receiveConsumerSimple key', msg);
    return msg;
  }

  /**
   *  * Consumer advance configuration and setting RETRY strategy with options retries
   *  * Example when consume process message then throw error by kafkaBroker library will handle with RecoverableError only
   *  *
   */
  @kafkaConsumer({
    groupId: `tel-topic-error-group`,
    topic: `tel-topic-error`,
    subscribe: {
      fromBeginning: true,
    },
    options: {
      allowAutoTopicCreation: true,
    },
    errorStrategy: {
      type: 'RETRY',
      retry: {
        retries: 3,
        initialRetryTime: 1000, // set delay retry
      },
    },
  })
  async receiveConsumerSimpleError(msg: ConsumeMessage) {
    console.log('🦻🦻🦻 receiveConsumerSimpleError working', msg);
    throw new RecoverableError('recoverable_type');
  }

  @kafkaProducer(`tel-topic-error-not-recover`)
  async sendProducerSimpleErrorNotRecover(
    order: IOrderSample,
  ): Promise<ProduceMessage> {
    return {
      message: order,
    };
  }

  /**
   * * Consumer not retry when throw error with UnrecoverableError or others error not match with function
   *
   */
  @kafkaConsumer({
    groupId: `tel-topic-error-group-not-recover`,
    topic: `tel-topic-error-not-recover`,
    subscribe: {
      fromBeginning: true,
    },
  })
  async receiveConsumerWithErrorNotHandler(msg: ConsumeMessage) {
    console.log('🦻🦻🦻 receiveConsumerWithErrorNotHandler working', msg);
    throw new UnrecoverableError('error_type_not_extend_recovers');
  }

  @kafkaProducer({
    topic: 'tel-topic-error-retry-forever',
  })
  async sendProducerSimpleErrorRetryForever(
    order: IOrderSample,
  ): Promise<ProduceMessage> {
    return {
      message: order,
    };
  }

  @kafkaConsumer({
    groupId: `tel-topic-error-group-forever`,
    topic: `tel-topic-error-retry-forever`,
    errorStrategy: {
      type: 'RETRY_FOREVER',
    },
  })
  async receiveConsumerSimpleErrorRetryForever(msg: ConsumeMessage) {
    console.log('🦻🦻🦻 receiveConsumerSimpleErrorRetryForever working', msg);
    if (counter > 10) {
      /** In case when counter > 10 we will allow commit message */
      return msg;
    } else {
      throw new RecoverableError(`🔥🔥 Failure to processing 😭`);
    }
  }

  @kafkaConsumer({
    groupId: `tel-topic-error-group-forever`,
    topic: `tel-topic-error-retry-forever`,
    errorStrategy: {
      type: 'RETRY_FOREVER',
      retry: {
        initialRetryTime: 300,
        maxRetryTime: 10000,
        factor: 0.2,
      },
    },
  })
  async receiveConsumerSimpleErrorRetryForeverWithAdvance(_: ConsumeMessage) {
    throw new RecoverableError(`🔥🔥 Failure to processing 😭`);
  }

  @kafkaConsumer({
    groupId: `tel-topic-dead-letter-group`,
    topic: `tel-topic-error-dead-letter`,
    subscribe: {
      fromBeginning: true,
    },
    errorStrategy: {
      type: 'RETRY_THEN_DEAD_LETTER',
      deadLetterTopic: 'tel-topic-dead-letter',
    },
  })
  async receiveConsumerErrorThenProduceDeadLetter(msg: ConsumeMessage) {
    console.log(
      '🦻🦻🦻 receiveConsumerErrorThenProduceDeadLetter working',
      msg,
    );
    throw new RecoverableError(`🔥🔥 Failure to processing 😭`);
  }

  @kafkaConsumer({
    groupId: `tel-topic-dead-letter-group`,
    topic: `tel-topic-error-dead-letter`,
    subscribe: {
      fromBeginning: true,
    },
    errorStrategy: {
      type: 'RETRY_THEN_DEAD_LETTER',
      deadLetterTopic: 'tel-topic-dead-letter',
      retry: {
        retries: 10,
        initialRetryTime: 500,
        maxRetryTime: 1000,
      },
    },
  })
  async receiveConsumerErrorThenProduceDeadLetterWithAdvance(
    msg: ConsumeMessage,
  ) {
    console.log(
      '🦻🦻🦻 receiveConsumerErrorThenProduceDeadLetter working',
      msg,
    );
    throw new RecoverableError(`🔥🔥 Failure to processing 😭`);
  }
}
