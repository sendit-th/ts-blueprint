import { Token } from 'typedi';

export interface KafkaConsumerFactory {
  create(): any;
}

export const KafkaConsumerFactory = new Token<KafkaConsumerFactory>(
  'KafkaConsumerFactory',
);
