import { ITask } from '@melonade/melonade-client';
import { mock, mockReset } from 'jest-mock-extended';
import { DemoDomain } from '../../../domains/demo/demo.domain';
import { DemoWorker } from './demo.worker';

const task: ITask = {
  transactionId: '1234',
  taskName: 'demo_worker',
  taskReferenceName: 'ref_demo_worker',
} as any;

describe('DemoWorker', () => {
  const domain = mock<DemoDomain>();
  const worker = new DemoWorker(domain);

  beforeEach(() => {
    mockReset(domain);
  });

  test('should process successfully', async () => {
    domain.findAll.mockResolvedValue([]);
    await worker.process(task);
    expect(domain.findAll).toBeCalled();
  });

  test('should compensate successfully', async () => {
    expect(await worker.compensate(task)).toBeTruthy();
  });
});
