import { ITask, ITaskResponse } from '@melonade/melonade-client';
import { Inject } from 'typedi';
import { DemoDomain } from '../../../domains/demo/demo.domain';
import { ILogger } from '../../../libraries/logger/logger.interface';
import { IMelonadeWorker } from '../../../libraries/worker/melonade';
import {
  workerCompleted,
  workerFailed,
} from '../../../libraries/worker/melonade/response';
import { consoleLogger } from '../../../logger';

export class DemoWorker implements IMelonadeWorker {
  constructor(
    private demoDomain: DemoDomain,
    @Inject('logger') private logger: ILogger = consoleLogger,
  ) {}

  get taskName() {
    return 'demo_worker';
  }

  async process(task: ITask): Promise<ITaskResponse> {
    const logMetadata = {
      event: task.taskReferenceName,
      workflowTransactionId: task.transactionId,
      workflowTaskName: task.taskName,
      workflowTaskReferenceName: task.taskReferenceName,
    };
    try {
      const demo = this.demoDomain.findAll();
      this.logger.info(logMetadata, `${JSON.stringify(demo)}`);
      return workerCompleted(demo);
    } catch (error) {
      return workerFailed(error);
    }
  }

  async compensate(task: ITask): Promise<ITaskResponse> {
    return workerCompleted(task);
  }
}
