import { IDemo, IDemoRequest } from '../../../domains/demo/interface';
import { buildSchema } from '../../../utils/mongoose/schemaBuilder';

const demoSchemaFields: Record<keyof IDemoRequest, any> = {
  name: { type: String, required: true },
  author: { type: String, required: false },
  authorEmail: { type: String, required: true },
  presentDate: { type: Date, required: true },
};

export const demoModel = buildSchema<IDemo>('Demo', demoSchemaFields);
