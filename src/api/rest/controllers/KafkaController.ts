import { Body, Controller, Post } from 'routing-controllers';
import { KafkaDemoService } from '../../../adapters/kafka/demo.kafka';

@Controller('/kafka')
export class KafkaController {
  constructor(private kafkaDemoService: KafkaDemoService) {}

  @Post('/error-dead-letter')
  async sendProducerSimpleDeadLetter(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimpleErrorDeadLetter(body);
    return 'ok';
  }

  @Post('/error-not-recover')
  async sendPublishErrorNotRecover(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimpleErrorNotRecover(body);
    return 'ok';
  }

  @Post('/error-forever')
  async sendProducerSimpleErrorRetryForever(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimpleErrorRetryForever(body);
    return 'ok';
  }

  @Post('/error')
  async sendProducerSimpleError(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimpleError(body);
    return 'ok';
  }

  @Post('/advance')
  async sendProducerSimpleWithKeyAndHeaders(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimpleWithKeyAndHeaders(body);
    return 'ok';
  }

  @Post('/simple')
  async sendPublish(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimple(body);
    return 'ok';
  }
}
