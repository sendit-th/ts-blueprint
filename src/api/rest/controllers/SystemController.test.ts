import { mock, mockReset } from 'jest-mock-extended';
import { HealthChecker } from '../../../libraries/health/healthchecker';
import { HealthController } from './SystemController';

describe('SystemController.test', () => {
  const mockHealthChecker = mock<HealthChecker>();
  const controller = new HealthController(mockHealthChecker);

  beforeEach(() => {
    mockReset(mockHealthChecker);
  });

  describe('Normal cases', () => {
    it('should be healthy', async () => {
      mockHealthChecker.check.mockResolvedValue({ ok: true });

      const mockCtx = { status: 200 } as any;
      const result = await controller.health(mockCtx);

      expect(result).toMatchObject({ ok: true });
      expect(mockCtx.status).toBe(200);
    });

    it('should be unhealthy', async () => {
      mockHealthChecker.check.mockResolvedValue({ ok: false });

      const mockCtx = { status: 200 } as any;
      const result = await controller.health(mockCtx);

      expect(result).toMatchObject({ ok: false });
      expect(mockCtx.status).toBe(503);
    });
  });
});
