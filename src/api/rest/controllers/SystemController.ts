import { IRouterContext } from 'koa-router';
import { Controller, Ctx, Get } from 'routing-controllers';
import { HealthChecker } from '../../../libraries/health/healthchecker';

@Controller('/system')
export class HealthController {
  constructor(private healthChecker: HealthChecker) {}

  @Get('/health')
  async health(@Ctx() ctx: IRouterContext) {
    const result = await this.healthChecker.check();

    if (!result.ok) {
      ctx.status = 503;
      ctx.message = 'Unhealthy';
    }

    return result;
  }
}
