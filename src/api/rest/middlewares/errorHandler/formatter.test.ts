import { ValidationError } from 'class-validator';
import { formatClassValidatorErrors } from './formatter';

function buildError(
  fieldName: string,
  errorName?: string,
  errorMessage?: string,
) {
  const error = new ValidationError();
  error.property = fieldName;

  if (errorName && errorMessage) {
    error.constraints = {
      [errorName]: errorMessage,
    };
  }
  return error;
}

describe('formatter.test', () => {
  describe('Normal cases', () => {
    it('should format validation successfully', () => {
      const error = buildError('field1', 'error1', 'Error 1');

      const formatted = formatClassValidatorErrors([error]);

      expect(formatted).toEqual(['Error 1']);
    });

    it('should format validation with nested errors successfully', () => {
      const error = buildError('field1');

      const childError = buildError(
        'childField1',
        'error1',
        'childFeidl1 has Error 1',
      );
      error.children = [childError];

      const formatted = formatClassValidatorErrors([error]);

      expect(formatted).toEqual(['[field1] childFeidl1 has Error 1']);
    });
  });
});
