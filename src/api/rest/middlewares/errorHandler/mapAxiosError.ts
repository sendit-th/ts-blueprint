import { AxiosError } from 'axios';
import { IErrorResponse } from '.';

export function mapAxiosError(error: Error): IErrorResponse | undefined {
  const axiosError = error as AxiosError;
  if (!axiosError.isAxiosError) return;

  const message = axiosError.message;
  const { url, method, baseURL } = axiosError.config;
  const absoluteUrl = `[${method?.toUpperCase()} method] ${baseURL}${url}`;

  if (axiosError.response) {
    const {
      response: { status },
    } = axiosError;
    const errorMessage = `Axios response error with status ${status} ${absoluteUrl} message ${message}`;
    return {
      status: status,
      body: {
        error: {
          message: errorMessage,
        },
      },
    };
  } else if (axiosError.request) {
    const { message, code } = axiosError;
    const errorMessage = `Axios request error with code ${code} ${absoluteUrl} message ${message}`;
    return {
      status: 400,
      body: {
        error: {
          message: errorMessage,
        },
      },
    };
  }

  const errorMessage = `Axios configuration error ${absoluteUrl} message ${message}`;
  return {
    status: 400,
    body: {
      error: {
        message: errorMessage,
      },
    },
  };
}
