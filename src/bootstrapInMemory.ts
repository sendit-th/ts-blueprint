import './adapters/mem/repositories'; // register all mongo repositories
import { defaultLogger } from './bootstrapLogger';

defaultLogger.info({ event: 'bootstrap_in_memory_repo' });
