import { SASLMechanism } from 'kafkajs';
import Container from 'typedi';
import './adapters/kafka';
import { KafkaConsumerFactory } from './adapters/kafka/factory';
import { config } from './bootstrapConfig';
import { defaultLogger } from './bootstrapLogger';
import {
  createKafkaConnection,
  IKafkaConnectionOptions,
} from './libraries/kafka';
import { gracefulShutdown } from './libraries/shutdown/gracefulShutdown';
import { ensureConfigKeys } from './utils/configUtil';

async function bootstrapKafka() {
  const logger = defaultLogger;

  try {
    if (config.kafka.KAFKA_SASL === true) {
      ensureConfigKeys(
        config.kafka,
        'KAFKA_BROKERS',
        'KAFKA_MECHANISM',
        'KAFKA_USERNAME',
        'KAFKA_PASSWORD',
      );
    } else {
      ensureConfigKeys(config.kafka, 'KAFKA_BROKERS');
    }

    const connectionUrl = config.kafka.KAFKA_BROKERS?.split(',') || [];

    // eslint-disable-next-line prefer-const
    let options: IKafkaConnectionOptions = {
      brokers: connectionUrl,
      topic_prefix: config.kafka.KAFKA_TOPIC_PREFIX || '',
    };

    if (
      config.kafka.KAFKA_MECHANISM &&
      config.kafka.KAFKA_USERNAME &&
      config.kafka.KAFKA_PASSWORD
    ) {
      options.sasl = {
        mechanism: config.kafka.KAFKA_MECHANISM as SASLMechanism,
        username: config.kafka.KAFKA_USERNAME,
        password: config.kafka.KAFKA_PASSWORD,
      };
    }

    if (config.kafka.KAFKA_SSL === true) {
      options.ssl = true;
    }

    const conn = await createKafkaConnection(options, logger);

    gracefulShutdown(logger, 'kafka', async () => {
      await conn.disconnect();
    });

    // register all consumers that create manually through KafkaConsumerFactory
    const kafkaConsumerFactories = Container.getMany(KafkaConsumerFactory);
    kafkaConsumerFactories.forEach((factory) => factory.create());

    await conn.consumeAllConsumers();

    logger.info(
      { event: 'bootstrap_kafka' },
      `Successfully connect to kafka server`,
    );
  } catch (error) {
    logger.error(
      error as any,
      { event: 'bootstrap_kafka' },
      `Error connected to kafka server`,
    );
    process.exit(-1);
  }
}

bootstrapKafka();
