import Container from 'typedi';
import { config } from './bootstrapConfig';
import { defaultLogger } from './bootstrapLogger';
import {
  IMelonadeWorker,
  IMelonadeWorkerOptions,
  initialMelonadeWorker,
} from './libraries/worker/melonade';
import { ensureConfigKeys } from './utils/configUtil';
import { importClassesFromDirectories } from './utils/importClassesFromDirectories';

async function bootstrapMelonadeWorker() {
  try {
    ensureConfigKeys(config.melonade, 'MELONADE_SERVER', 'MELONADE_NAMESPACE');

    const path = __dirname + '/adapters/melonade/workers/!(*.test.*)';
    const workerClasses = importClassesFromDirectories([path]);

    const workerInstances = workerClasses.map((worker) =>
      Container.get<IMelonadeWorker>(worker),
    );

    defaultLogger.info({
      event: 'get_worker_instances',
      instances: workerInstances.length,
    });

    const options: IMelonadeWorkerOptions = {
      workers: workerInstances,
      servers: config.melonade.MELONADE_SERVER || '',
      namespace: config.melonade.MELONADE_NAMESPACE || '',
    };

    await initialMelonadeWorker(options, defaultLogger);

    defaultLogger.info(
      { event: 'bootstrap_melonade_worker' },
      `Successfully connect to melonade server`,
    );
  } catch (error) {
    defaultLogger.error({ err: error, event: 'bootstrap_melonade_worker' });
    process.exit(-1);
  }
}

bootstrapMelonadeWorker();
