import { RequestError } from '../../errors/errors';

export class PastDateError extends RequestError {
  constructor(datename: string, date: Date) {
    super(
      `Date could not be in the past: ${datename} (${date.toDateString()})`,
    );
  }
}
