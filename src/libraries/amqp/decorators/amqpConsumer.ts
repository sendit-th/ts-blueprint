import { MetaDataRegistry } from '../metaDataRegistry';
import { IQueueConfig, ISubscriptionConfig } from '../rascalConfig';

export function amqpConsume(
  queueName: string,
  subConfig?: ISubscriptionConfig,
): Function;
export function amqpConsume(
  queueConfig: IQueueConfig,
  subConfig?: ISubscriptionConfig,
): Function;
export function amqpConsume(
  queueNameOrConfig: string | IQueueConfig,
  subConfig?: ISubscriptionConfig,
) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor,
  ) {
    if (typeof descriptor.value === 'function') {
      let config: IQueueConfig = {} as any;

      if (typeof queueNameOrConfig === 'string') {
        config.queueName = queueNameOrConfig;
      } else {
        config = queueNameOrConfig;
      }

      MetaDataRegistry.REGISTER_CONSUMER(
        config,
        target.constructor,
        propertyKey,
        subConfig,
      );
    }
  };
}
