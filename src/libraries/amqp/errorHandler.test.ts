import { Message } from 'amqplib';
import { handleOnMessageError } from './errorHandler';
import { InvalidMessage } from './errorType';
import {
  ErrorHandlingStrategy,
  FORWARD,
  REJECT,
  REJECT_WITH_ERROR_INFO,
  REPUBLISH,
  REQUEUE,
} from './type';

describe('errorHandler function', () => {
  const throwError = new Error('test-error');
  const ackOrNack = jest.fn();
  const messageMock: Message = {
    fields: {
      consumerTag: 'amq.test-tag',
      deliveryTag: 1,
      redelivered: false,
      exchange: '',
      routingKey: 'development.demo_named_queue',
    },
    properties: {
      contentType: 'application/json',
      contentEncoding: undefined,
      headers: {
        'x-correlation-id': '74bb3ae2-ad11-4d16-86a6-ce7ae599e95d',
      },
      deliveryMode: 2,
      priority: undefined,
      correlationId: undefined,
      replyTo: undefined,
      expiration: undefined,
      messageId: 'f444cfc7-0d8b-41d2-9d7a-51d9e4171fe4',
      timestamp: undefined,
      type: undefined,
      userId: undefined,
      appId: undefined,
      clusterId: 'clusterId-test',
    },
    content: Buffer.from(JSON.stringify({ message: 'test-message' })),
  };

  beforeEach(() => {
    ackOrNack.mockClear();
  });

  it('REPUBLISH strategy', () => {
    const strategy: ErrorHandlingStrategy = { type: REPUBLISH };
    handleOnMessageError(throwError, ackOrNack, messageMock, strategy);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining({ strategy: 'republish', defer: 1000 }),
    );
  });

  it('REPUBLISH strategy with attempts', () => {
    const strategy: ErrorHandlingStrategy = { type: REPUBLISH, attempts: 10 };
    handleOnMessageError(throwError, ackOrNack, messageMock, strategy);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining([
        { strategy: 'republish', defer: 1000, attempts: 10 },
        { strategy: 'nack' },
      ]),
    );
  });

  it('REPUBLISH strategy when throw error instanceof InvalidMessage', () => {
    const strategy: ErrorHandlingStrategy = { type: REPUBLISH };
    const throwError = new InvalidMessage();
    handleOnMessageError(throwError, ackOrNack, messageMock, strategy);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining({ strategy: 'republish', immediateNack: true }),
    );
  });

  it('default strategy', () => {
    handleOnMessageError(throwError, ackOrNack, messageMock);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining([
        { strategy: 'republish', defer: 1000, attempts: 10 },
        { strategy: 'nack' },
      ]),
    );
  });

  it('FORWARD strategy', () => {
    const strategy: ErrorHandlingStrategy = {
      type: FORWARD,
      exchange: 'test-exchange',
    };
    handleOnMessageError(throwError, ackOrNack, messageMock, strategy);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining({
        strategy: 'forward',
        publication: 'test-exchange',
      }),
    );
  });

  it('FORWARD strategy already attempts', () => {
    const strategy: ErrorHandlingStrategy = {
      type: FORWARD,
      exchange: 'test-exchange',
      attempts: 10,
    };
    handleOnMessageError(throwError, ackOrNack, messageMock, strategy);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining([
        {
          strategy: 'forward',
          publication: 'test-exchange',
          defer: 1000,
          attempts: 10,
        },
        { strategy: 'nack' },
      ]),
    );
  });

  it('REQUEUE strategy', () => {
    const strategy: ErrorHandlingStrategy = { type: REQUEUE };
    handleOnMessageError(throwError, ackOrNack, messageMock, strategy);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining({ strategy: 'nack', defer: 1000, requeue: true }),
    );
  });

  it('REJECT strategy', () => {
    const strategy: ErrorHandlingStrategy = { type: REJECT };
    handleOnMessageError(throwError, ackOrNack, messageMock, strategy);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining({ strategy: 'nack' }),
    );
  });

  it('REJECT_WITH_ERROR_INFO strategy', () => {
    const strategy: ErrorHandlingStrategy = { type: REJECT_WITH_ERROR_INFO };
    handleOnMessageError(throwError, ackOrNack, messageMock, strategy);
    expect(ackOrNack).toHaveBeenCalled();
    expect(ackOrNack.mock.calls[0][1]).toEqual(
      expect.objectContaining({ strategy: 'republish', immediateNack: true }),
    );
  });
});
