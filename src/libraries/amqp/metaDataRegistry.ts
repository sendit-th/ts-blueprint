import {
  IPublicationConfig,
  IQueueConfig,
  ISubscriptionConfig,
} from './rascalConfig';
import { IConsumerMeta, IPublisherMeta } from './type';

export class MetaDataRegistry {
  static consumerMetas: IConsumerMeta[] = [];
  static publisherMetas: IPublisherMeta[] = [];

  static REGISTER_CONSUMER(
    queueConfig: IQueueConfig,
    type: object,
    propertyKey: string,
    subConfig?: ISubscriptionConfig,
  ) {
    MetaDataRegistry.consumerMetas.push({
      type,
      propertyKey,
      queueConfig,
      subConfig,
    });
  }

  static REGISTER_PUBLISHER(pubConfig: IPublicationConfig) {
    MetaDataRegistry.publisherMetas.push({ pubConfig });
  }
}
