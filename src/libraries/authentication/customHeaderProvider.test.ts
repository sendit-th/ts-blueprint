import { mock, mockClear } from 'jest-mock-extended';
import { AuthenticationProvider } from './authenticationProvider';
import { AuthenticationError } from './errors';
import { IAnonoymousableUser, IUserRepository } from './type';

interface ITestUser extends IAnonoymousableUser {}

describe('Authentication Provider', () => {
  const mockUserRepo = mock<IUserRepository<ITestUser>>();

  let provider: AuthenticationProvider<ITestUser>;

  beforeEach(() => {
    mockClear(mockUserRepo);
  });

  describe('Custom Header', () => {
    it('should returns user when header contains default headers', async () => {
      mockUserRepo.getUserById.mockResolvedValue({ anonymous: false });

      provider = new AuthenticationProvider(mockUserRepo, {
        useCustomHeader: true,
      });

      const result = await provider.getUserFromCtx({
        headers: { 'x-consumer-username': 'user1' },
      });

      expect(result).toBeTruthy();
      expect(result?.anonymous).toBe(false);
      expect(mockUserRepo.getUserById).toBeCalledWith('user1');
    });

    it('should returns user when header contains custom headers', async () => {
      mockUserRepo.getUserById.mockResolvedValue({ anonymous: false });

      provider = new AuthenticationProvider(mockUserRepo, {
        useCustomHeader: true,
        customHeaderOptions: { userIdKey: 'x-custom-username' },
      });

      const result = await provider.getUserFromCtx({
        headers: { 'x-custom-username': 'user1' },
      });

      expect(result).toBeTruthy();
      expect(result?.anonymous).toBe(false);
      expect(mockUserRepo.getUserById).toBeCalledWith('user1');
    });

    it('should returns user when header contains default annonymous header', async () => {
      mockUserRepo.getUserById.mockResolvedValue({ anonymous: false });

      provider = new AuthenticationProvider(mockUserRepo, {
        useCustomHeader: true,
        allowAnonymous: true,
      });

      const result = await provider.getUserFromCtx({
        headers: {
          'x-consumer-username': 'user1',
          'x-anonymous-consumer': 'true',
        },
      });

      expect(result).toBeTruthy();
      expect(result?.anonymous).toBe(true);
      expect(mockUserRepo.getUserById).toBeCalledWith('user1');
    });

    it('should throw error when header contains annonymous header but not allowed', async () => {
      mockUserRepo.getUserById.mockResolvedValue({ anonymous: false });

      provider = new AuthenticationProvider(mockUserRepo, {
        useCustomHeader: true,
        allowAnonymous: false,
      });

      const check = () => {
        return provider.getUserFromCtx({
          headers: {
            'x-consumer-username': 'user1',
            'x-anonymous-consumer': 'true',
          },
        });
      };

      await expect(check()).rejects.toThrowError(AuthenticationError);
    });
  });
});
