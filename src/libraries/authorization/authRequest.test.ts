import { buildAuthorizationRequest } from './authRequest';

describe('Authorization request', () => {
  it('should return authenticated request when data is empty array', () => {
    const request = buildAuthorizationRequest<any>([]);

    expect(request.authenticated).toBe(true);
  });

  it('should return role based request when data is an array of string', () => {
    const request = buildAuthorizationRequest<any>(['role1', 'role2']);

    expect(request.roles).toEqual(['role1', 'role2']);
  });

  it('should return role based request when data contains permission', () => {
    const request = buildAuthorizationRequest<any>([
      { scope: 'read', resource: 'a' },
    ]);

    expect(request.permission).toEqual({ scope: 'read', resource: 'a' });
  });

  it('should return role based request when data contains any scopes permission', () => {
    const request = buildAuthorizationRequest<any>([{ resource: 'a' }]);

    expect(request.permission).toEqual({ scope: '*', resource: 'a' });
  });

  it('should return role based request when data contains any resources permission', () => {
    const request = buildAuthorizationRequest<any>([{ scope: 'read' }]);

    expect(request.permission).toEqual({ scope: 'read', resource: '*' });
  });
});
