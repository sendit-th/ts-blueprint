import { RoleAuthorizationProvider } from './roleProvider';
import { IAuthorizationRequest } from './type';

interface IUser {
  roles: string[];
}

describe('Role Authorization Provider', () => {
  it('should support', () => {
    const provider = new RoleAuthorizationProvider();
    const request: IAuthorizationRequest<any> = {
      roles: ['role1'],
    };

    expect(provider.isSupported(request)).toBe(true);
  });

  it('should not support', () => {
    const provider = new RoleAuthorizationProvider();
    const request: IAuthorizationRequest<any> = {
      roles: [],
    };

    expect(provider.isSupported(request)).toBe(false);
  });

  it('should authorized when user has role', () => {
    const provider = new RoleAuthorizationProvider();
    const request: IAuthorizationRequest<IUser> = {
      roles: ['role1'],
      user: {
        roles: ['role1'],
      },
    };

    expect(provider.isAuthorized(request)).resolves.toBe(true);
  });

  it('should authorized when user has any roles', () => {
    const provider = new RoleAuthorizationProvider();
    const request: IAuthorizationRequest<IUser> = {
      roles: ['role1', 'role2'],
      user: {
        roles: ['role2'],
      },
    };

    expect(provider.isAuthorized(request)).resolves.toBe(true);
  });

  it('should not authorized when user does not have specific role', () => {
    const provider = new RoleAuthorizationProvider();
    const request: IAuthorizationRequest<IUser> = {
      roles: ['role1', 'role2'],
      user: {
        roles: ['role3'],
      },
    };

    expect(provider.isAuthorized(request)).resolves.toBe(false);
  });
});
