import { IAuthorizationProvider, IAuthorizationRequest } from './type';

export interface IUserWithRole {
  roles: string[];
}

export class RoleAuthorizationProvider<U extends IUserWithRole>
  implements IAuthorizationProvider<U> {
  isSupported(request: IAuthorizationRequest<U>): boolean {
    if (request.roles && request.roles.length > 0) {
      return true;
    }

    return false;
  }

  async isAuthorized(request: IAuthorizationRequest<U>): Promise<boolean> {
    // user has any role -> permit
    const { user } = request;
    if (user && user.roles) {
      for (const userRole of user.roles) {
        if (request.roles?.includes(userRole)) {
          return true;
        }
      }
    }

    return false;
  }
}
