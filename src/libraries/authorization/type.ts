export interface IPermission {
  resource: string;
  scope: string;
}

export interface IAuthorizationOption {
  authenticated?: boolean;
  roles?: string[];
  scope?: string;
  resource?: string;
}

export interface IAuthorizationRequest<U> {
  authenticated?: boolean;
  user?: U;
  roles?: string[];
  permission?: IPermission;
}
export interface IAuthorizationRequest<U> {
  authenticated?: boolean;
  user?: U;
  roles?: string[];
  permission?: IPermission;
}

export interface IAuthorizationProvider<U> {
  isSupported(request: IAuthorizationRequest<U>): boolean;
  isAuthorized(request: IAuthorizationRequest<U>): Promise<boolean>;
}

export interface IPermissionRepository<U> {
  getPermissions(user: U): Promise<IPermission[] | undefined>;
}

export interface IAuthorizationOptions {}
