import * as cacheManager from 'cache-manager';
import { ILogger } from '../../logger/logger.interface';
import { ICacheConfig, ICacheProvider } from '../interface';

export interface IInMemoryCacheOptions {
  ttl: number;
  max?: number;
}

export class InMemoryCacheProvider implements ICacheProvider {
  cache: cacheManager.Cache;

  constructor(logger: ILogger, options: IInMemoryCacheOptions) {
    logger.debug('InMemoryCacheProvider with options: ', options);

    this.cache = cacheManager.caching({
      store: 'memory',
      max: options.max,
      ttl: options.ttl,
    });
  }

  wrap(key: string, cb: () => any, option?: ICacheConfig): Promise<any> {
    return this.cache.wrap(key, cb, option as any);
  }

  del(key: string, cb: (err: any) => void) {
    return this.cache.del(key, cb);
  }
}
