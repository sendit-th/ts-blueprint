import { mock } from 'jest-mock-extended';
import { consoleLogger } from '../../logger';
import { KafkaConnection } from './connection';
import { IConsumer, IKafkaBroker } from './kafkaBroker';
import { MetaDataRegistry } from './metaDataRegistry';

describe('Kafka connection', () => {
  it('should create KafkaConnection instead successfully', () => {
    const brokerMock = mock<IKafkaBroker>();
    const conn = new KafkaConnection(brokerMock, consoleLogger);
    expect(conn).toBeDefined();
    expect(KafkaConnection.connection instanceof KafkaConnection).toBe(true);
  });

  it('should produce via KafkaConnection instead successfully', async () => {
    const brokerMock = mock<IKafkaBroker>();
    const conn = new KafkaConnection(brokerMock, consoleLogger);
    const callbackFn = jest.fn();
    await KafkaConnection.PRODUCE({ topic: 'test-topic' }, callbackFn, [], {});

    expect(conn).toBeDefined();
    expect(KafkaConnection.connection instanceof KafkaConnection).toBe(true);
    expect(brokerMock.produce).toBeCalled();
  });

  it('should call consume all from MetadataRegistry consumerMetas', async () => {
    class TestMock {
      consume() {}
    }
    MetaDataRegistry.REGISTER_CONSUMER(
      {
        groupId: 'test',
        subscribe: {
          topic: 'test-topic-1',
        },
      },
      TestMock,
      'consume',
    );

    MetaDataRegistry.REGISTER_CONSUMER(
      {
        groupId: 'test',
        subscribe: {
          topic: 'test-topic-2',
        },
      },
      TestMock,
      'consume',
    );

    const brokerMock = mock<IKafkaBroker>();
    const conn = new KafkaConnection(brokerMock, consoleLogger);
    brokerMock.consume.mockResolvedValue({} as IConsumer);
    await conn.consumeAllConsumers();

    expect(conn).toBeDefined();
    expect(KafkaConnection.connection instanceof KafkaConnection).toBe(true);
    expect(brokerMock.consume).toBeCalled();
    expect(brokerMock.consume).toBeCalledTimes(2);
  });
});
