/* eslint-disable @typescript-eslint/ban-ts-comment */
import { ConsumeMessage } from '../kafkaBroker';
import { MetaDataRegistry } from '../metaDataRegistry';
import { kafkaConsumer } from './kafkaConsumer';

jest.mock('../metaDataRegistry.ts');

describe('Kafka consumer decorator', () => {
  //@ts-ignore
  class TestClass {
    @kafkaConsumer({
      groupId: 'test-consumer-group',
      topic: 'test-topic',
      subscribe: {
        fromBeginning: true,
      },
    })
    async consumer(msg: ConsumeMessage) {
      return msg;
    }

    @kafkaConsumer({
      groupId: 'test-consumer-group',
      topic: 'test-topic-2',
      errorStrategy: {
        type: 'RETRY_THEN_DEAD_LETTER',
        deadLetterTopic: 'test-topic-retry',
        retry: {
          retries: 3,
        },
      },
    })
    async consumer2(msg: ConsumeMessage) {
      return msg;
    }
  }

  describe('Build config', () => {
    const mockRegisterConsumer = MetaDataRegistry.REGISTER_CONSUMER as jest.Mock;

    it('Can build config total consumer decorator successfully', () => {
      expect(mockRegisterConsumer).toHaveBeenCalledTimes(2);
    });

    it(`Can build config ready default 'RETRY' error strategy`, () => {
      expect(mockRegisterConsumer.mock.calls[0]).toEqual([
        {
          errorStrategy: {
            type: 'RETRY',
          },
          retry: {},
          groupId: 'test-consumer-group',
          subscribe: {
            fromBeginning: true,
            topic: 'test-topic',
          },
          topic: 'test-topic',
        },
        TestClass,
        'consumer',
      ]);
    });

    it(`Can build config ready default 'RETRY_THEN_DEAD_LETTER' error strategy`, () => {
      expect(mockRegisterConsumer.mock.calls[1]).toEqual([
        {
          errorStrategy: {
            deadLetterTopic: 'test-topic-retry',
            retry: {
              retries: 3,
            },
            type: 'RETRY_THEN_DEAD_LETTER',
          },
          groupId: 'test-consumer-group',
          retry: {
            retries: 3,
          },
          subscribe: {
            fromBeginning: false,
            topic: 'test-topic-2',
          },
          topic: 'test-topic-2',
        },
        TestClass,
        'consumer2',
      ]);
    });
  });
});
