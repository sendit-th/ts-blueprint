import { ConsumerConfig, ConsumerSubscribeTopic, RetryOptions } from 'kafkajs';
import { MetaDataRegistry } from '../metaDataRegistry';
import { ErrorHandlingStrategy } from '../type';

interface IConsumerConfig extends Omit<ConsumerConfig, 'groupId' | 'retry'> {}
export interface IKafkaConsumer {
  topic: string;
  groupId: string;
  subscribe?: {
    fromBeginning: boolean;
  };
  options?: IConsumerConfig;
  errorStrategy?: ErrorHandlingStrategy;
}

export interface IKafkaConsumerConfig extends ConsumerConfig {
  subscribe?: ConsumerSubscribeTopic;
  errorStrategy?: ErrorHandlingStrategy;
}

type KafkaConsumerDecorator = (
  target: Object,
  propertyKey: string,
  descriptor: TypedPropertyDescriptor<(...args: any[]) => Promise<any>>,
) => void;

export function buildKafkaConsumerConfig(
  consumerConfig: IKafkaConsumer,
): IKafkaConsumerConfig {
  const defaultErrorStrategy: ErrorHandlingStrategy = { type: 'RETRY' };

  const { errorStrategy = defaultErrorStrategy } = consumerConfig;

  const subscribe: ConsumerSubscribeTopic = {
    fromBeginning: consumerConfig.subscribe?.fromBeginning || false,
    topic: consumerConfig.topic,
  };

  const retry: RetryOptions = {
    ...errorStrategy.retry,
  };

  if (errorStrategy?.type === 'RETRY_FOREVER') {
    retry.retries = Number.POSITIVE_INFINITY;
  }

  return {
    ...consumerConfig,
    subscribe,
    retry,
    errorStrategy,
  };
}

export function kafkaConsumer(
  consumerConfig: IKafkaConsumer,
): KafkaConsumerDecorator {
  return function (target, propertyKey) {
    const consumerConfigOverride = buildKafkaConsumerConfig(consumerConfig);

    MetaDataRegistry.REGISTER_CONSUMER(
      consumerConfigOverride,
      target.constructor,
      propertyKey,
    );
  };
}
