export class KafkaErrorRetry extends Error {}

export class KafkaErrorRetriesExhausted extends Error {}

export class KafkaInvalidTopic extends Error {}

export class RecoverableError extends Error {}

export class UnrecoverableError extends Error {}

export enum NetworkErrorCodes {
  ETIMEDOUT = 'ETIMEDOUT',
  ECONNRESET = 'ECONNRESET',
  EADDRINUSE = 'EADDRINUSE',
  ECONNREFUSED = 'ECONNREFUSED',
  EPIPE = 'EPIPE',
  ENOTFOUND = 'ENOTFOUND',
  ENETUNREACH = 'ENETUNREACH',
  EAI_AGAIN = 'EAI_AGAIN',
}

export const ErrorResponseStatusCodes = [
  408,
  413,
  429,
  500,
  502,
  503,
  504,
  521,
  522,
  524,
];
