import { KafkaConfig } from 'kafkajs';
import { ILogger } from '../logger/logger.interface';
import { KafkaConnection } from './connection';
import { KafkaBroker } from './kafkaBroker';

export interface IKafkaConnectionOptions extends KafkaConfig {
  topic_prefix?: string;
}

export async function createKafkaConnection(
  options: IKafkaConnectionOptions,
  logger: ILogger,
) {
  if (!options.topic_prefix) {
    Object.assign(options, { topic_prefix: '' });
  }

  const broker = new KafkaBroker(options, logger);
  await broker.connect();

  return new KafkaConnection(broker, logger);
}
