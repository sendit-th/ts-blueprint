/* eslint-disable @typescript-eslint/no-unused-vars */
import { AxiosError } from 'axios';
import { Consumer, Kafka, KafkaMessage, Message, Transaction } from 'kafkajs';
import { mongo } from 'mongoose';
import { Service } from 'typedi';
import { IKafkaConnectionOptions } from '.';
import { handleOnMessageError } from '../kafka/errorHandler';
import { invokePromiseOrFunction, safeJSONParse } from '../kafka/util';
import { ILogger } from '../logger/logger.interface';
import { IKafkaConsumerConfig } from './decorators/kafkaConsumer';
import { IKafkaProducer } from './decorators/kafkaProducer';
import {
  ErrorResponseStatusCodes,
  KafkaInvalidTopic,
  NetworkErrorCodes,
  RecoverableError,
} from './errorTypes';
import { PayloadHandler } from './payloadHandler';

export interface IConsumer {
  connect(): Promise<void>;
  disconnect(): Promise<void>;
}

export interface IKafkaBroker {
  connect(): Promise<void>;
  disconnect(): Promise<void>;
  toBufferJSON(value: any): Buffer;
  prepareProduceMessage(config: IKafkaProducer, messages: any): Message;
  serialize(
    config: IKafkaProducer,
    messages: ProduceMessage | ProduceMessage[],
  ): Message[];
  deSerialize(message: KafkaMessage): IMessageConsumer;
  produceTransaction(
    producerConfig: IKafkaProducer,
    transactionalId: string,
    message: any | any[],
    callBackFunction: Function,
  ): Promise<void>;
  produce(producerConfig: IKafkaProducer, message: any | any[]): Promise<void>;
  consume(
    consumerConfig: IKafkaConsumerConfig,
    originalFunction: Function,
    context: any,
  ): Promise<IConsumer>;
}

export interface IMessageConsumer extends KafkaMessage {
  value: any;
  config?: IKafkaProducer;
}

export interface ProduceMessage {
  key?: any;
  headers?: Record<string, string>;
  message: any;
  config?: IKafkaProducer;
}

export type ConsumeMessage = IMessageConsumer;

@Service()
export class KafkaBroker implements IKafkaBroker {
  private broker!: Kafka;
  private consumers: Consumer[] = [];

  constructor(
    private kafkaConfig: IKafkaConnectionOptions,
    private logger: ILogger,
  ) {}

  async connect() {
    const { topic_prefix, ...config } = this.kafkaConfig;
    this.broker = new Kafka({
      ...config,
    });

    const producer = this.broker.producer();
    await producer.connect();
    this.logger.info(
      { event: 'kafka_broker_connected' },
      `Successfully connect to kafka broker`,
    );
  }

  toBufferJSON(value: Record<string, any> | string): Buffer {
    return Buffer.from(JSON.stringify(value));
  }

  prepareProduceMessage(_: IKafkaProducer, messages: ProduceMessage): Message {
    let value = messages;
    let payload: Record<string, any>;

    if (typeof messages === 'object' && 'message' in messages) {
      value = messages.message;
      payload = { value: this.toBufferJSON(value) };

      if (messages.key)
        payload = { ...payload, key: this.toBufferJSON(messages.key) };

      if (
        typeof messages.headers === 'object' &&
        Object.keys(messages.headers).length > 0
      ) {
        payload = { ...payload, headers: messages.headers };
      }
    } else {
      payload = { value: this.toBufferJSON(value) };
    }

    return payload as Message;
  }

  serialize(
    config: IKafkaProducer,
    messages: ProduceMessage | ProduceMessage[],
  ): Message[] {
    let messageSerialize: Message[] = [];

    if (Array.isArray(messages)) {
      for (const message of messages) {
        const payload = this.prepareProduceMessage(config, message);
        messageSerialize.push(payload);
      }
    } else {
      const payload = this.prepareProduceMessage(config, messages);
      messageSerialize = [payload];
    }
    return messageSerialize;
  }

  async produceTransaction(
    producerConfig: IKafkaProducer,
    transactionalId: string,
    message: any | any[],
    callBackFunction: Function,
  ) {
    const producer = this.broker.producer({
      maxInFlightRequests: 1,
      idempotent: true,
      transactionalId,
    });

    await producer.connect();
    const transaction: Transaction = await producer.transaction();
    const messages = this.serialize(producerConfig, message);

    if (!producerConfig.topic) {
      throw new KafkaInvalidTopic('Invalid topic for produce');
    }

    try {
      await transaction.send({
        topic: producerConfig.topic,
        messages,
      });

      await callBackFunction();

      transaction.commit();
      this.logger.info({
        event: 'kafka_transaction_commit',
        topic: producerConfig.topic,
      });
    } catch (error) {
      await transaction.abort();
      this.logger.error(error, { event: 'kafka_transaction_rollback' });
      throw error;
    }
  }

  async produce(producerConfig: IKafkaProducer, message: any) {
    const { config, options } = producerConfig;
    const topic = `${this.kafkaConfig.topic_prefix}${producerConfig.topic}`;
    const messages: Message[] = this.serialize(producerConfig, message);

    await this.broker.producer(config).send({
      topic,
      messages,
      ...options,
    });
  }

  deSerialize(message: KafkaMessage): IMessageConsumer {
    let messageDeSerialize: IMessageConsumer = message;

    if (message.value instanceof Buffer) {
      const plainMessage = message.value.toString();
      const { value } = safeJSONParse(plainMessage);
      messageDeSerialize = Object.assign({}, messageDeSerialize, {
        value: value,
      });
    }

    if (message.key instanceof Buffer) {
      const plainMessage = message.key.toString();
      const { value } = safeJSONParse(plainMessage);
      messageDeSerialize = Object.assign({}, messageDeSerialize, {
        key: value,
      });
    }

    if (
      typeof message.headers === 'object' &&
      Object.keys(message.headers).length > 0
    ) {
      const headers = message.headers;
      const _headers: any = {};
      Object.keys(headers).forEach((key) => {
        _headers[key] = headers[key].toString();
      });

      messageDeSerialize = Object.assign({}, messageDeSerialize, {
        headers: _headers,
      });
    }
    return messageDeSerialize;
  }

  async disconnect() {
    await Promise.all(this.consumers.map((consumer) => consumer.disconnect()));
  }

  async consume(
    consumerConfig: IKafkaConsumerConfig,
    originalFunction: Function,
    context: any,
  ) {
    const consumer = this.broker.consumer(consumerConfig);
    this.consumers.push(consumer);

    await consumer.connect();
    await consumer.subscribe(consumerConfig.subscribe!);

    await consumer.run({
      autoCommit: false,
      eachMessage: async (payload) => {
        const payloadHandler = new PayloadHandler(
          consumerConfig,
          consumer,
          payload,
        );

        const { topic, partition, message } = payload;
        const messageDeSerialize = this.deSerialize(message);

        try {
          await invokePromiseOrFunction(originalFunction, context, [
            messageDeSerialize,
          ]);
          await payloadHandler.commit();

          this.logger.trace({
            event: 'kafka_commit',
            topic,
            partition,
            message,
          });
        } catch (error) {
          this.logger.error({
            event: 'kafka_consume_error',
            error_type: 'pre_retry_process',
            err: error,
            ...payloadHandler.getConsumerMeta(),
          });

          if (this.isRecoverable(error)) {
            await handleOnMessageError(
              error,
              payloadHandler,
              this.logger,
              this,
              consumerConfig.errorStrategy,
            );
          } else {
            this.logger.error({
              event: 'kafka_error',
              error_type: 'failure_retry_process',
              ...payloadHandler.getConsumerMeta(),
            });
            await payloadHandler.commit();
          }
        }
      },
    });

    return consumer;
  }

  isRecoverable(error: Error): boolean {
    if (
      error instanceof mongo.MongoError ||
      error instanceof RecoverableError
    ) {
      return true;
    }

    if ((error as AxiosError).isAxiosError) {
      const errorAxios = error as AxiosError;
      if (
        Object.values(NetworkErrorCodes).includes(
          errorAxios.code as NetworkErrorCodes,
        )
      ) {
        return true;
      }

      if (ErrorResponseStatusCodes.includes(errorAxios.response?.status || 0)) {
        return true;
      }
    }

    return false;
  }
}
