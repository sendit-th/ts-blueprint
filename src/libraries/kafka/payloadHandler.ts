import { Consumer } from 'kafkajs';
import { EachMessagePayload } from 'kafkajs/types';
import { IKafkaConsumerConfig } from './decorators/kafkaConsumer';

const store: any = {};

export class PayloadHandler {
  constructor(
    private consumerConfig: IKafkaConsumerConfig,
    private consumer: Consumer,
    private payload: EachMessagePayload,
  ) {}

  getPayload(): EachMessagePayload {
    return this.payload;
  }

  getId(): string {
    return `${this.consumerConfig.groupId}-${this.payload.topic}-${this.payload.partition}-${this.payload.message.offset}`;
  }

  getConsumerMeta() {
    return {
      groupId: this.consumerConfig.groupId,
      topic: this.payload.topic,
      partition: this.payload.partition,
      offset: this.payload.message.offset,
    };
  }

  increaseRetry() {
    const key = this.getId();
    store[key] = store[key] || 0;
    store[key] += 1;
  }

  retryCount() {
    const key = this.getId();
    return store[key] || 0;
  }

  clearRetry() {
    const key = this.getId();
    delete store[key];
  }

  async commit() {
    return this.consumer.commitOffsets([
      {
        topic: this.payload.topic,
        partition: this.payload.partition,
        offset: String(parseInt(this.payload.message.offset) + 1),
      },
    ]);
  }

  toString() {
    return `GroupID : ${this.consumerConfig.groupId} Topic: ${this.payload.topic} Partition: ${this.payload.partition} Offset: ${this.payload.message.offset}`;
  }
}
