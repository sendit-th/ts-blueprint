import { RetryOptions } from 'kafkajs';

export interface RetryForeverOptions extends Omit<RetryOptions, 'retries'> {}

export const RETRY = 'RETRY';
export const RETRY_THEN_DEAD_LETTER = 'RETRY_THEN_DEAD_LETTER';
export const RETRY_FOREVER = 'RETRY_FOREVER';

export interface RetryStrategy {
  type: typeof RETRY;
  retry?: RetryOptions;
}

export interface RetryThenDeadLetterStrategy {
  type: typeof RETRY_THEN_DEAD_LETTER;
  retry?: RetryOptions;
  deadLetterTopic: string;
}

export interface RetryForeverStrategy {
  type: typeof RETRY_FOREVER;
  retry?: RetryForeverOptions;
}

export type ErrorHandlingStrategy =
  | RetryStrategy
  | RetryThenDeadLetterStrategy
  | RetryForeverStrategy;
