# How to use json logger
## Info level
Use when logging general information or when a transaction is process successfully.
```js
log.info('hi');                     // {msg: 'hi'}
log.info({event: 'test'});          // {event: 'test'}
log.info({foo: 'bar'}, 'hi');       // {foo: 'bar', msg: 'hi'}
log.info({foo: 'bar'}, {event: 'e1'});       // {foo: 'bar', msg: '{event: "e1"}} - note that msg is string
```

## Error level
Use when logging error information. All the errors should be logged.
```js
const error = new Error('test error');

log.error(error);                     // {err: {message: 'test error', stack: '...error stack'}}
log.error({err: error}, 'test');      // {err: {message: 'test error', stack: '...error stack'}, msg: 'test'}
log.error({err: error, event: 'e1'}, 'test');      // {event: 'e1', err: {message: 'test error', stack: '...error stack'}, msg: 'test'}

// Special for ts-blueprint!!!
log.error(err, {event: 'e1'}, 'test');             // {event: 'e1', err: {message: 'test error', stack: '...error stack'}, msg: 'test'}
```

## Debug level
Use when logging diagnotic information such as application internal state. This level should be able to turn on or off via configuration.
```js
log.debug('debug message');          // {msg: 'debug message'}
log.debug({event: 'e1'}, 'debug message');       // {event: 'e1', msg: 'debug message'}
```

## Warning level
Use when logging an event that could cause problem but not a severe one. For example, the use of deprecated code.
```js
log.warn('warning message');          // {msg: 'warning message'}
log.warn({event: 'e1'}, 'warning message');       // {event: 'e1', msg: 'warning message'}
```

# <span style="color:red">WARNING!!!</span>
- Do not put a large object such as 'body' or 'data' as object (first parameter). This could cause issue on log aggregation server like elasticsearch.