import { ITask, ITaskResponse, Worker } from '@melonade/melonade-client';
import { ILogger } from '../../logger/logger.interface';

export interface IMelonadeWorkerOptions {
  workers: IMelonadeWorker[];
  servers: string;
  namespace: string;
}

export interface IMelonadeWorker {
  readonly taskName: string;
  process: (task: ITask) => Promise<ITaskResponse>;
  compensate: (task: ITask) => Promise<ITaskResponse>;
}

export async function initialMelonadeWorker(
  options: IMelonadeWorkerOptions,
  logger: ILogger,
) {
  options.workers?.forEach((worker) => {
    const taskName = worker.taskName;
    const workerInstance = new Worker(
      taskName,
      (task) => worker.process(task),
      (task) => worker.compensate(task),
      {
        kafkaServers: options.servers,
        namespace: options.namespace,
      },
    );
    workerInstance.once('ready', () => {
      logger.info(
        { events: 'melonade_client_connection' },
        `worker ${taskName} is ready`,
      );
    });
  });
}
