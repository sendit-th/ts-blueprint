import { ITaskResponse, TaskStates } from '@melonade/melonade-client';

export const workerCompleted = (output?: any): ITaskResponse => {
  return {
    status: TaskStates.Completed,
    output: output,
  };
};

export const workerFailed = (output?: any): ITaskResponse => {
  return {
    status: TaskStates.Failed,
    output: output,
  };
};

export const workerInProgress = (output?: any): ITaskResponse => {
  return {
    status: TaskStates.Inprogress,
    output: output,
  };
};
