export function ensureConfigKey<T>(c: T, key: keyof T) {
  if (!c[key]) {
    throw new Error(`config ${key.toString()} is required`);
  }
}

export function ensureConfigKeys<T>(c: T, ...keys: (keyof T)[]) {
  for (const key of keys) {
    ensureConfigKey(c, key);
  }
}
