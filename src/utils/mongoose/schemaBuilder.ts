/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Document,
  ToObjectOptions,
  model,
  Schema,
  SchemaDefinition,
  SchemaOptions,
} from 'mongoose';

/**
 * apply function transform to toObject() to convert _id from ObjectID to string
 */
const toObjectOptions: ToObjectOptions = {
  transform: (
    _doc: any,
    ret: { _id: { toString: () => any } },
    _options: any,
  ) => {
    if (ret._id) {
      ret._id = ret._id.toString();
    }
    return ret;
  },
};

const defaultSchemaOptions: SchemaOptions = {
  timestamps: true,
  toObject: toObjectOptions,
};

export function buildSchema<T>(
  name: string,
  schemaDef: SchemaDefinition,
  additionalSchemaOptions?: SchemaOptions,
) {
  const options = Object.assign(
    {},
    defaultSchemaOptions,
    additionalSchemaOptions,
  );

  const schema = new Schema(schemaDef, options);

  return model<T & Document>(name, schema);
}
